<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use function Sodium\add;

/**
 * Companies Model
 *
 * @property \App\Model\Table\ItemsTable|\Cake\ORM\Association\HasMany $Items
 *
 * @method \App\Model\Entity\Company get($primaryKey, $options = [])
 * @method \App\Model\Entity\Company newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Company[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Company|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Company|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Company patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Company[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Company findOrCreate($search, callable $callback = null, $options = [])
 */
class CompaniesTable extends Table {

    private const FIXED_LINE_PHONE_NUMBER = '/^0(\d{1}-\d{4}|\d{2}-\d{3}|\d{3}-\d{2}|\d{4}-\d{1})-\d{4}$/';
    private const CELL_PHONE_NUMBER = '/^0[5789]0-\d{4}-\d{4}$/';
    private const FREE_PHONE_NUMBER = '/^0120-\d{3}-\d{3}$/';

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('companies');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Items', [
            'foreignKey' => 'company_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('phone_number')
            ->maxLength('phone_number', 14)
            ->requirePresence('phone_number', 'create')
            ->notEmpty('phone_number')
            ->add('phone_number', 'validatePhoneNumber',
                [
                    'rule'     => ['validatePhoneNumber'],
                    'message'  => __('ハイフン込みの電話番号を入力してください'),
                    'provider' => 'table'
                ]
            );

        return $validator;
    }

    public function validatePhoneNumber($data) {
        // 固定電話
        if (preg_match(self::FIXED_LINE_PHONE_NUMBER, $data, $matches))
            return true;

        // 携帯電話
        if (preg_match(self::CELL_PHONE_NUMBER, $data, $matches))
            return true;

        // フリーダイアル
        if (preg_match(self::FREE_PHONE_NUMBER, $data, $matches))
            return true;

        return false;
    }

}
