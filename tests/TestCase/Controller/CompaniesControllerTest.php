<?php

namespace App\Test\TestCase\Controller;

use App\Controller\CompaniesController;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\CompaniesController Test Case
 */
class CompaniesControllerTest extends IntegrationTestCase {

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = ['app.companies', 'app.items'];
    private $companies;
    private $initialCount = null;

    public function __construct(?string $name = null, array $data = [], string $dataName = '') {
        parent::__construct($name, $data, $dataName);
        //DB接続を取得
        $connection = ConnectionManager::get('test');
        //SQLのログ出力を有効化
        $connection->logQueries(true);

        $this->companies = TableRegistry::getTableLocator()
                                        ->get('Companies');
    }

    public function setUp() {
        parent::setUp();
        $this->companies = TableRegistry::getTableLocator()
                                        ->get('Companies');
        $this->initialCount = $this->companies->find()
                                              ->count();
    }

    public function tearDown() {
        parent::tearDown();
        $this->initialCount = null;
    }

    /**
     * Test view method
     *
     * @return void
     * @throws \PHPUnit\Exception
     */
    public function testViewNonNullId() {
        $this->get('/companies/view/1');
        $this->assertResponseOk();
    }

    /**
     * Test view method
     *
     * @return void
     * @throws \PHPUnit\Exception
     */
    public function testViewNullId() {
        $this->get('/companies/view');
        $this->assertResponseError();
    }

    /**
     * Test add method
     *
     * @return void
     * @throws \PHPUnit\Exception
     */
    public function testAddValidData() {
        $data = [
            'name'         => 'GM',
            'phone_number' => '080-1234-1234'
        ];
        // CSRFトークンを利用する
        // これがないとテストがこける
        $this->enableCsrfToken();
        $this->enableSecurityToken();
        $this->post('/companies/add', $data);
        $this->assertRedirect(['action' => 'index']);

        $this->assertEquals($this->initialCount + 1, $this->companies->find()
                                                                     ->count());
    }

    /**
     * Test add method
     *
     * @return void
     * @throws \PHPUnit\Exception
     */
    public function testAddInValidData() {
        $data = [
            'name'         => 'GM',
            'phone_number' => '0801234-1234'
        ];
        $this->enableCsrfToken();
        $this->enableSecurityToken();
        $this->post('/companies/add', $data);
        $a = $this->companies->find();
        $this->assertEquals($this->initialCount, $this->companies->find()
                                                                 ->count());
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete() {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
