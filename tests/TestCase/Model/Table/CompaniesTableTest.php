<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CompaniesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CompaniesTable Test Case
 */
class CompaniesTableTest extends TestCase {

    /**
     * Test subject
     *
     * @var \App\Model\Table\CompaniesTable
     */
    public $Companies;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = ['app.companies', 'app.items'];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Companies') ? [] : ['className' => CompaniesTable::class];
        $this->Companies = TableRegistry::getTableLocator()->get('Companies', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown() {
        unset($this->Companies);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidatePhoneNumber() {
        // 固定電話番号のテスト
        $this->assertEquals(true, $this->Companies->validatePhoneNumber('01-2345-6789'));
        $this->assertEquals(true, $this->Companies->validatePhoneNumber('012-345-6789'));
        $this->assertEquals(true, $this->Companies->validatePhoneNumber('0123-45-6789'));
        $this->assertEquals(true, $this->Companies->validatePhoneNumber('01234-5-6789'));
        // 携帯電話番号のテスト
        $this->assertEquals(true, $this->Companies->validatePhoneNumber('050-1234-6789'));
        $this->assertEquals(true, $this->Companies->validatePhoneNumber('070-1234-6789'));
        $this->assertEquals(true, $this->Companies->validatePhoneNumber('080-1234-6789'));
        $this->assertEquals(true, $this->Companies->validatePhoneNumber('090-1234-6789'));
        // フリーダイアルのテスト
        $this->assertEquals(true, $this->Companies->validatePhoneNumber('0120-123-467'));

        $this->assertEquals(false, $this->Companies->validatePhoneNumber('012(345)6789'));
        $this->assertEquals(false, $this->Companies->validatePhoneNumber(' 060-1234-6789'));
        $this->assertEquals(false, $this->Companies->validatePhoneNumber('060-1234-6789'));
        $this->assertEquals(false, $this->Companies->validatePhoneNumber('01201234-6789'));
    }
}
