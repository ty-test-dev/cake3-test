<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CompaniesFixture
 *
 */
class CompaniesFixture extends TestFixture {

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id'           => ['type'          => 'integer',
                           'length'        => 11,
                           'unsigned'      => false,
                           'null'          => false,
                           'default'       => null,
                           'comment'       => '',
                           'autoIncrement' => true,
                           'precision'     => null],
        'name'         => ['type'      => 'string',
                           'length'    => 100,
                           'null'      => false,
                           'default'   => null,
                           'collate'   => 'utf8mb4_bin',
                           'comment'   => '',
                           'precision' => null,
                           'fixed'     => null],
        'phone_number' => ['type'      => 'string',
                           'length'    => 14,
                           'null'      => false,
                           'default'   => null,
                           'collate'   => 'utf8mb4_bin',
                           'comment'   => '',
                           'precision' => null,
                           'fixed'     => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options'     => [
            'engine'    => 'InnoDB',
            'collation' => 'utf8mb4_bin'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init() {
        $this->records = [
            [
                'name'         => 'aiueo',
                'phone_number' => '0234-56-7891'
            ],
        ];
        parent::init();
    }
}
