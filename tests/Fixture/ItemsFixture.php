<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ItemsFixture
 *
 */
class ItemsFixture extends TestFixture {

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id'           => ['type'          => 'integer',
                           'length'        => 11,
                           'unsigned'      => false,
                           'null'          => false,
                           'default'       => null,
                           'comment'       => '',
                           'autoIncrement' => true,
                           'precision'     => null],
        'name'         => ['type'          => 'integer',
                           'length'        => 11,
                           'unsigned'      => false,
                           'null'          => false,
                           'default'       => null,
                           'comment'       => '',
                           'precision'     => null,
                           'autoIncrement' => null],
        'price'        => ['type'          => 'integer',
                           'length'        => 11,
                           'unsigned'      => false,
                           'null'          => false,
                           'default'       => null,
                           'comment'       => '',
                           'precision'     => null,
                           'autoIncrement' => null],
        'description'  => ['type'      => 'text',
                           'length'    => null,
                           'null'      => false,
                           'default'   => null,
                           'collate'   => 'utf8mb4_bin',
                           'comment'   => '',
                           'precision' => null],
        'company_id'   => ['type'          => 'integer',
                           'length'        => 11,
                           'unsigned'      => false,
                           'null'          => true,
                           'default'       => null,
                           'comment'       => '',
                           'precision'     => null,
                           'autoIncrement' => null],
        '_indexes'     => [
            'item_compan_id' => ['type' => 'index', 'columns' => ['company_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary'        => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'item_compan_id' => ['type'       => 'foreign',
                                 'columns'    => ['company_id'],
                                 'references' => ['companies', 'id'],
                                 'update'     => 'cascade',
                                 'delete'     => 'setNull',
                                 'length'     => []],
        ],
        '_options'     => [
            'engine'    => 'InnoDB',
            'collation' => 'utf8mb4_bin'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init() {
        $this->records = [
            [
                'id'          => 1,
                'name'        => 1,
                'price'       => 1,
                'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'company_id'  => 1
            ],
        ];
        parent::init();
    }
}
